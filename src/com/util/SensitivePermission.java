package com.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class SensitivePermission {
	private static boolean LOAD_SENSITIVE_PERMISSION = false;
	private static boolean GENERATE_PERMISSION_NAME_MAP = false;
	
	private static ArrayList <Permission> permissionArrayList = new ArrayList <Permission>();
	private static HashMap<String, String> hashMap = new HashMap<String, String>();
	
	/*
	 * get sensitive Api(s) Array List
	 */
	public static ArrayList <String> getSensitiveApiList()
	{
		ArrayList <String> aList = new ArrayList <String>();
		
		//load Sensitive Permission List
		loadSensitivePermissionList();
		
		Iterator<Permission> pait = permissionArrayList.iterator();
		while (pait.hasNext())
		{
			Permission per = pait.next();
			//stringArray2ArrayList(per.getSensitiveApi());
			aList.addAll(stringArray2ArrayList(per.getSensitiveApi()));
		}
		
		//printArrayList(aList);//debug it
		
		return aList;
	}
	
	/*
	 * get CN_Name from permission
	 */
	public static String getNameFromPermission(String permission)
	{
		generatePermission2NameMap();
		
		return hashMap.get(permission).toString();
	}
	
	/*
	 * generate Permission To CN_Name hash Map
	 */
	public static void generatePermission2NameMap()
	{
		if (GENERATE_PERMISSION_NAME_MAP)
		{
			return;
		}
		
		//load Sensitive Permission List 
		loadSensitivePermissionList();
		
		Iterator<Permission> pList = permissionArrayList.iterator();
		while (pList.hasNext())
		{
			Permission pm = pList.next();
			hashMap.put(pm.getPermissionName(), pm.getCN_Name());
		}
		
		GENERATE_PERMISSION_NAME_MAP = true;
	}
	
	/*
	 * find SensitivePermission for apk File
	 */
	public static ArrayList<String> getSensitivePermission(ArrayList<String> apkPermission)
	{
		ArrayList<String> al = new ArrayList<String>();
		
		//load SensitivePermission file
		loadSensitivePermissionList();
		
		Iterator<Permission> its = permissionArrayList.iterator();
		while (its.hasNext())
		{
			Permission it = its.next();
			String tmp = it.getPermissionName();
			if (apkPermission.contains(tmp))
			{
				al.add(tmp);
			}
		}
		
		return al;
	}
	
	/*
	 * load profile about sensitive api
	 */
	private static void loadSensitivePermissionList()
	{
		if (LOAD_SENSITIVE_PERMISSION)
		{
			return;
		}
		
		String location = "resource/sensitivePermission.json";
//		Permission permission = new Permission();
//		
//		permission.setCN_Name("发短信");
//		permission.setPermissionName("permisison.sendSMS");
//		String[] api = {"sendtext()", "ssss"};
//		permission.setSensitiveApi(api);
//		
//        String json = FastJSONHelper.serialize(permission);
//        System.out.println(json);
		
		File file = new File(location);
		String json = null;
		try {
			json = FileUtils.readFileToString(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
		JSONObject jo = JSON.parseObject(json);
		JSONArray ja = jo.getJSONArray("permission");
		//System.out.println("---->" + ja.size());
		
		for (int i=0; i<ja.size(); ++i)
		{
			JSONObject jao = ja.getJSONObject(i);
			Permission permission = new Permission();
			
			//create Permission class
			permission.setPermissionName(jao.getString("permissionName"));
			permission.setCN_Name(jao.getString("cN_Name"));
			
			JSONArray apiArray = jao.getJSONArray("sensitiveApi");
			String tmp = "";
			for (int j=0; j<apiArray.size(); ++j)
			{
				//System.out.println(".....>" + apiArray.get(j).toString());
				tmp = tmp +  apiArray.get(j).toString() + "---";
			}
			
			permission.setSensitiveApi(tmp.split("---"));
			//debug it
//			System.out.println("--->" + permission.getPermissionName());
//			System.out.println("--->" + permission.getCN_Name());
//			String[] str = permission.getSensitiveApi();
//			for (int k=0; k<str.length; ++k)
//			{
//				System.out.println("--->yuyifei:" + str[k]);
//			}
			
			permissionArrayList.add(permission);
		}
		
		LOAD_SENSITIVE_PERMISSION = true;
	}
	
	/*
	 * use aapt tool to acquire Apk file permission list
	 */
	public static ArrayList<String> getApkPermissionList(String apkPath)
	{
		String cmd = "tools/aapt dump permissions " + apkPath;
		ArrayList<String> al = null;
		
		try {
			//System.out.println("cmd:" + cmd);
			Process p = Runtime.getRuntime().exec(cmd);
			
			int processExitCode = p.waitFor();
			if (processExitCode != 0) 
			{
				throw new RuntimeException("Something went wrong during acquire apk permission(tools:aapt)");
			}
			
			al = getAaptResult(p);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e){
			e.printStackTrace();
		}
		
		//printArrayList(al);
		
		return al;
	}
	
	/*
	 * after run the aapt, get the output result from terminal
	 */
	private static ArrayList<String> getAaptResult(Process p) throws IOException{
		String line;
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		ArrayList<String> al = new ArrayList<String>();
		
		while ((line = input.readLine()) != null) 
		{
		  //System.out.println(line);
		  
		  if (line.startsWith("uses-permission"))
		  {
			  //System.out.println(line);
			  String userPermission;
			  String[] str = line.split("'");
			  //System.out.println(line.split("'").length);
			  if (str.length != 2)
			  {
				  System.out.println("parser permission error");
			  }
			  //System.out.println("+++++++++" + str[1]);
			  userPermission = str[1];
			  al.add(userPermission);
		  }
		}
		input.close();
		
		//findSensitivePermission(al);
		
		input = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		while ((line = input.readLine()) != null) 
		{
		  System.out.println("It appears error information ...");
		}
		input.close();
		
		return al;
	}
	
	/*
	 * print a array list for debug
	 */
	public static void printArrayList(ArrayList<String> arrayList)
	{
		Iterator<String> alist = arrayList.iterator();
		
		while (alist.hasNext())
		{
			String al = alist.next();
			
			System.out.println(al);
		}
	}
	
	/*
	 *  ArrayList<String> transform String[]
	 */
	private static String[] ArrayList2StringArray(ArrayList<String> als)
	{
		String[] sa = new String[als.size()];
		als.toArray(sa);
		
		return sa;
	}
	
	/*
	 *  String[] transform ArrayList<String>
	 */
	private static ArrayList<String> stringArray2ArrayList(String[] sa)
	{
		ArrayList<String> als=new ArrayList<String>(0);
		
		for (int i=0; i<sa.length; ++i)
		{
			als.add(sa[i]);
		}
		
		return als;
	}
}
