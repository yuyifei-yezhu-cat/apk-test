package com.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;

import android.content.res.AXmlResourceParser;

public class RunHelpClass 
{
	/*
	 * anti-debug(modify AndroidManifest)
	 * android:debuggable=true ---> android:debuggable=false
	 */
	public static File modifyAndroidDebugAttr(File apkFile)
	{
		File newFile = null;
		
		if (!apkFile.exists())
		{
			System.err.println("error: can not find apk file, AndroidManifest.xml can not be modified.");
			System.exit(1);
		}
		
		//delete decode
		File decodedDir = new File ("decoded");
		if (decodedDir.exists() && decodedDir.isDirectory()) 
		{
			try {
				FileUtils.deleteDirectory(decodedDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			newFile = parser(apkFile);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return newFile;
	}
	
	private static File parser(File apkFile) throws IOException, InterruptedException
	{
		System.out.println("--->" + apkFile.getAbsolutePath());
		
		Process p = Runtime.getRuntime().exec(
					"java -jar tools/apktool.jar d -s -f " + apkFile.getAbsolutePath() + " -o decoded");
		
		System.out.println("java -jar tools/apktool.jar d -s -f " + apkFile.getAbsolutePath() + " -o decoded");
		
		int processExitCode = p.waitFor();
		
		if (processExitCode != 0) 
		{
			throw new RuntimeException("Something went wrong during unpacking");
		}
		
		String androidManifestxml = "decoded/AndroidManifest.xml";
		
		boolean flag = false;
		try {
			flag = replaceDebugger(androidManifestxml);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		
		if (!flag)
		{
			throw new RuntimeException("modify AndroidManifest.xml failed.");
		}
		
		return packApk(apkFile);
	}
	
	

	private static File packApk(File apkFile) throws IOException, InterruptedException 
	{
		// Now pack again
		File newFile = new File(apkFile.getAbsolutePath().replace(".apk", "") + "_modifyDebug.apk");
		if (newFile.exists())
		{
			newFile.delete();
		}
		
		Process p = Runtime.getRuntime().exec("java -jar tools/apktool.jar b decoded/ -o " + newFile.getAbsolutePath());
		int processExitCode = p.waitFor();
		if (processExitCode != 0)
		{
			System.err.println("Something went wrong when packing " + apkFile.getAbsolutePath());
			return null;
		}
		
		return newFile;
	}

	private static boolean replaceDebugger(String androidManifestxml) 
			throws ParserConfigurationException, SAXException, IOException, 
			XPathExpressionException, TransformerException 
	{
		
		BufferedReader br = new BufferedReader(new FileReader(androidManifestxml));
		StringBuffer sb = new StringBuffer();
		String line = "";
		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}
		br.close();
		String manifest = sb.toString();
		
		// Do some XML parsing
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new ByteArrayInputStream(manifest.getBytes()));
		doc.getDocumentElement().normalize();
		
		XPath xpath = XPathFactory.newInstance().newXPath();
        XPathExpression expr1 = xpath.compile("//manifest/application/@debuggable");
        NodeList nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        
        ((Attr) nodes.item(0)).setNodeValue("false");//set the attr :false  eg:android:debuggable="true"
        
        //write to new AndroidManifest.xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        File file = new File(androidManifestxml);
        OutputStream out = new FileOutputStream(file, false);
		StreamResult result = new StreamResult(out);
        transformer.transform(source, result);

		return true;
	}

	/*
	 * modify AndroidManfest.xml
	 * add My ContentProvider permission to this apk file
	 * 1、use apktool decode it 
	 * 2、parser the AndroidManifest.xml
	 * 3、add my ContentProvider permission
	 */
	public static File modifyAndroidManifestXml(File file)
	{
		File newFile = file;
		
		if (!newFile.exists())
		{
			System.err.println("error: can not find apk file, can not be modified.");
			System.exit(1);
		}
		
		//Add permission to AndroidManifest.xml
		HashMap<String, String> replacements = new HashMap<String, String>();
		replacements.put("</manifest>", "<uses-permission android:name=\"android.permission.WRITE_EXTERNAL_STORAGE\" /> </manifest>");
		//replacements.put("</application>", "<provider android:authorities=\"com.example.provider.College\" android:name=\"MyPrint\"/> </application>");
		
		newFile = ParserApk.adaptManifest(file.getAbsolutePath(), replacements);	
		
		return newFile;
	}
	
	/*
	 * sign and zipalign a apk file
	 */
	public static File signAndZipalign(File file)
	{
		File newFile = file;
		
		if (!newFile.exists())
		{
			System.err.println("error: can not find apk file, can not be sign and zipalign.");
			System.exit(1);
		}
		
		//apk sign and zipalign
        try {
        	newFile = apkSign(file);//apk sign
        	newFile = apkZipalign(newFile);//apk zipalign
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        
        return newFile;
	}
	
	private static File apkZipalign(File apkFile) throws IOException, InterruptedException 
	{
		System.out.println("------" + apkFile.getAbsolutePath());
		
		File newFile = new File(apkFile.getAbsolutePath().replace(".apk", "") + "_zipalign.apk");
		
		Process p = Runtime.getRuntime().exec("tools/zipalign -f -v -z 4 "
											+ apkFile.getAbsolutePath() + " "
											+ newFile.getAbsolutePath());
		
		System.out.println("tools/zipalign -f -v -z 4 "
					+ apkFile.getAbsolutePath() + " "
					+ newFile.getAbsolutePath());
		
		int processExitCode = p.waitFor();
		if (processExitCode != 0)
		{
			System.out.println("Something went wrong when zipaligning " + apkFile.getAbsolutePath());
			return null;
		}
			
		return newFile;
	}
	
	private static File apkSign(File apkFile) throws IOException, InterruptedException
	{
		String relativeLocation = "tools/apk-sign";
		
		System.out.println("------" + apkFile.getAbsolutePath());
		
		File newFile = new File(apkFile.getAbsolutePath().replace(".apk", "") + "_signed.apk");
		
		Process p = Runtime.getRuntime().exec(
									"java -jar " 
									+ relativeLocation + "/signapk.jar "
									+ relativeLocation + "/testkey.x509.pem "
									+ relativeLocation + "/testkey.pk8 "
									+ apkFile.getAbsolutePath() + " "
									+ newFile.getAbsolutePath());
		
		System.out.println("java -jar " 
				+ relativeLocation + "/signapk.jar "
				+ relativeLocation + "/testkey.x509.pem "
				+ relativeLocation + "/testkey.pk8 "
				+ apkFile.getAbsolutePath() + " "
				+ newFile.getAbsolutePath());
				
		int processExitCode = p.waitFor();
		if (processExitCode != 0)
		{
			System.out.println("Something went wrong when signing " + apkFile.getAbsolutePath());
			return null;
		}
		
		return newFile;
	}
}
