package com.instrument;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.io.FileUtils;
import com.control.ArgumentParser;
import com.util.RunHelpClass;
import com.util.SensitivePermission;

public class Main 
{
	public final static String apkPath = "/home/yuyifei/Desktop/githhub-paper/android-instrumentation-tutorial/app-example/RV2013/bin/RV2013.apk";
//	public final static String apkPath = "/home/yuyifei/Downloads/yuyifei/ReadSMS/bin/ReadSMS.apk";

	public static void main(String[] args)
	{
		ArrayList<String> apkPermissionArray = null;
		ArrayList<String> sensitivePermissionArray = null;
		
		//get the permission list from a  apk file
		apkPermissionArray = SensitivePermission.getApkPermissionList(apkPath);
		System.out.println("\n\n=============apk permission=============");
		SensitivePermission.printArrayList(apkPermissionArray);
		
		//get the sensitive permission list
		sensitivePermissionArray = SensitivePermission.getSensitivePermission(apkPermissionArray);
		System.out.println("\n\n=============sensitive permission=============");
		System.out.println("Warn:find sensitive permission number:" + sensitivePermissionArray.size());
		SensitivePermission.printArrayList(sensitivePermissionArray);
		//if size=0; return;
		if (sensitivePermissionArray.size()<1)
		{
			return;
		}
		
		//argument control eg:
    	ArgumentParser ap = new ArgumentParser();
    	ap.parser(args);
		
		//First, delete sootOutput dir
		File sootOutputDir = new File ("sootOutput");
		if (sootOutputDir.exists() && sootOutputDir.isDirectory()) 
		{
			try {
				FileUtils.deleteDirectory(sootOutputDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//run the soot for instrument 
		SootOption.runSoot();
		
		String outputApk = "sootOutput/"+new File(apkPath).getName();
		File file = new File(outputApk);
		
		//add permission to AndroidManifest.xml
		//file = RunHelpClass.modifyAndroidManifestXml(file);
		
		//apk file sign and Zipalign
		file = RunHelpClass.signAndZipalign(file);
		System.out.println("generate apk file:" + file.getAbsolutePath());
		System.out.println("Game over ...");
	}
	
}
