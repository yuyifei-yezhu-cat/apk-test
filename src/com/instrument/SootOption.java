package com.instrument;

import java.io.File;
import java.util.Collections;

import soot.G;
import soot.PackManager;
import soot.Scene;
import soot.SootClass;
import soot.Transform;
import soot.options.Options;

public class SootOption 
{
	public static void runSoot()
	{
		
		Config config = Global.getConfig();
		
		String androidJARPath = config.getAndroidJARPath();//android jar path
		String androidJAR = config.getAndroidJAR();//force android.jar path
		String jdkPath = config.getJdkPath();//jdk rt.jar path
		
		String[] sootArgs = {"-process-dir", Main.apkPath};
		
		G.reset();//init soot
		
		Options.v().set_soot_classpath(jdkPath + File.pathSeparator + androidJAR 
				+ File.pathSeparator + "./src" + File.pathSeparator + Main.apkPath);
		
		Options.v().set_android_jars(androidJARPath);
		//Options.v().set_force_android_jar(androidJAR);
		
		Options.v().set_allow_phantom_refs(true);
		
		Options.v().set_exclude(Collections.singletonList("android.support.v4."));
		
		Options.v().set_src_prec(Options.src_prec_apk);
		
		//Options.v().set_process_dir(Collections.singletonList(Main.apkPath));
		
		Options.v().set_output_format(Options.output_format_dex);
		//Options.v().set_output_format(Options.output_format_jimple);
		
		Options.v().set_validate(true);
		
		Options.v().keep_line_number();
		
		Options.v().keep_offset();
		
        // resolve the PrintStream and System soot-classes
        Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
        Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
        Scene.v().addBasicClass("android.widget.Toast", SootClass.SIGNATURES);
        Scene.v().addBasicClass("android.view.View", SootClass.SIGNATURES);
        Scene.v().addBasicClass("com.util.InstrumentHelpClass", SootClass.SIGNATURES);
        
        PackManager.v().getPack("jtp").add(new Transform("jtp.myInstrumenter", new MyBodyTransformer()));
      
        soot.Main.main(sootArgs);
	}
}
