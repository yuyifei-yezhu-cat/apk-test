package com.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;

public class InstrumentHelpClass 
{
	/*
	 * certificate fingerprint authentication
	 */
	public static void certFingerprintAuthentication(Context context, String packageName, String fingerprint)
	{
		PackageManager packageManager = context.getPackageManager();
		
		try {
			PackageInfo packageInfo = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
			Signature[] signs = packageInfo.signatures;
			Signature sign = signs[0];
			
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
			X509Certificate cert = (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(sign.toByteArray()));
			
			byte[] myCert = cert.getEncoded();
	        MessageDigest digest = null;
			try {
				digest = java.security.MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
	        digest.update(myCert);
	        byte messageDigest[] = digest.digest();
	        
	         // Create Hex String
	         StringBuffer hexString = new StringBuffer();
	         for (int i=0; i<messageDigest.length; i++)
	             hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
	         
	         if (hexString.toString().equals(fingerprint))
	         {
	        	 System.out.println("cert_Md5:" + hexString.toString());
	        	 System.out.println("Have the same fingerprint.");
	         }
	         else
	         {
	        	 System.out.println("android_cert_Md5:" + hexString.toString());
	        	 System.out.println("java_cert_Md5:" + fingerprint);
	        	 System.err.println("Have the same fingerprint.");
	         }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * anti debug
	 */
	public static void antiDebug(Context context)
	{
		ApplicationInfo appInfo = context.getApplicationInfo();
		
		if ((appInfo.flags &= 
        		ApplicationInfo.FLAG_DEBUGGABLE) != 0){
        	Log.e("com.security.antidebug", "AndroidManifest debug status is : true. ");
        	//android.os.Process.killProcess(android.os.Process.myPid());
        }  
        if (android.os.Debug.isDebuggerConnected()) {
        	Log.e("com.security.antidebug", "debugger is connected.");
        	//android.os.Process.killProcess(android.os.Process.myPid());
        }  
	}
	
	/*
	 * anti emulator code
	 */
	public static void isRunningInEmualtor() 
	{
    	boolean qemuKernel = false;
    	Process process = null;
        DataOutputStream os = null;
        try{  
            process = Runtime.getRuntime().exec("getprop ro.kernel.qemu");  
            os = new DataOutputStream(process.getOutputStream());
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream(),"GBK"));
            os.writeBytes("exit\n");  
            os.flush();
            process.waitFor();
            qemuKernel = (Integer.valueOf(in.readLine()) == 1);
            Log.d("com.security.checkqemu", "find emulator:" + qemuKernel);  
            
            if (qemuKernel){
            	Log.e("com.security.checkqemu", "warn: find emulator.");
            	//android.os.Process.killProcess(android.os.Process.myPid());
            }
        } catch (Exception e){  
        	qemuKernel = false;
            Log.d("com.security.checkqemu", "run failed" + e.getMessage()); 
        } finally {
            try{  
                if (os != null) {  
                    os.close();  
                }  
                process.destroy();  
            } catch (Exception e) {
            	
            }  
            Log.d("com.security.checkqemu", "run finally"); 
        }
    }
	
	
	/*
	 * Android Alert Dialog
	 */
	public static void instrumentAlertDialog(Context context)
	{
		CharSequence title = "余奕飞";
		CharSequence message = "有人偷发短信";
		CharSequence button = "确定";
		OnClickListener listen = null;
		
		new AlertDialog.Builder(context)
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(button, listen)
		.show();
	}
}
