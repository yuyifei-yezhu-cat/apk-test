package com.control;

public class args {
	   public enum ArgType {
		   ARGTYPE_BOOL,
		   ARGTYPE_EMPTY, // expects no argument
		   ARGTYPE_INT,
		   ARGTYPE_STRING,
	   }	   
	   public String argName;
	   public String argDisplay;
	   public String argDescription;
	   public ArgType argType;
	   public F action;
	   public args(String argName, String argDisplay, String argDescription, ArgType argType, 
			   F action){
		   this.argName = argName;
		   this.argDisplay = argDisplay;
		   this.argDescription = argDescription;
		   this.argType = argType;
		   this.action = action;
	   }
	   @Override
	   public String toString(){
		   String s = " -" + argName + " ";
		   if (argDisplay != "")
			   s = s + argDisplay + " ";
		   s = s + argDescription + " ";
		   if (!this.argType.toString().equals("ARGTYPE_EMPTY"))
			   s = s + this.argType.toString();
		   return s;
	   }
	}
