#!/bin/sh


#define some var
#APK_NAME=ReadSMS
#APK_PACKAGE=com.yuyifei
#MAIN_ACTIVITY=MainActivity

APK_NAME=RV2013
APK_PACKAGE=de.ecspride
MAIN_ACTIVITY=RV2013 

#rootPath=/home/yuyifei/Downloads/yuyifei/apk-test
rootPath=/home/yuyifei/git/apk-test

#uninstall the apk file
echo "uninstall the apk file"
adb uninstall $APK_PACKAGE

#re-install apk
echo "re-install apk file ..."
#adb install $rootPath/sootOutput/$APK_NAME\_modified_signed_zipalign.apk
adb install $rootPath/sootOutput/$APK_NAME\_signed_zipalign.apk

#start a activity
echo "start apk main activity"
adb shell am start -n $APK_PACKAGE/.$MAIN_ACTIVITY
