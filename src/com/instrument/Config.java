package com.instrument;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config 
{
	private static String defaultAndroidJARPath = "resource/android-platforms-master";
	private static String defaultAndroidJAR = "resource/android-platforms-master/android-17/android.jar";
	private static String defaultJDKPath = "resource/rt.jar";

	private String androidJARPath = null;
	private String androidJAR = null;
	private String jdkPath = null;
	
	private static Config config;

	public static Config g() 
	{
		if (config == null)
			config = new Config();
		return config;
	}
	
	public Config() 
	{
		Properties props = new Properties();
		try {
			props.load(new FileInputStream("resource/apktest.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.androidJARPath = props.getProperty("apk.androidPath.jar", Config.defaultAndroidJARPath);
		this.androidJAR = props.getProperty("apk.android.jar", Config.defaultAndroidJAR);
		this.jdkPath = props.getProperty("apk.jdk.jar", Config.defaultJDKPath);
		
		//****************Debug it***************//
		System.out.println("----> :" + this.androidJARPath);
		System.out.println("----> :" + this.androidJAR);
		System.out.println("----> :" + this.jdkPath);
	}
	
	public String getAndroidJARPath() {
		return androidJARPath;
	}

	public String getAndroidJAR() {
		return androidJAR;
	}

	public String getJdkPath() {
		return jdkPath;
	}
}
