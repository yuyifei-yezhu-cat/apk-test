package com.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class ApkInformation 
{
	static boolean LOADAPKINFORMATION = false;
	
	private static String packageName = null;
	private static String fingerprint = null;
	
	public static String getPackageName() {
		return packageName;
	}

	public static String getFingerprint() {
		return fingerprint;
	}

	
	public static void getApkInfo(String apkPath)
	{
		if ( LOADAPKINFORMATION)
		{
			System.out.println("apk info has alreadly load.");
			return;
		}
		
		setFingerprint();//load fingerprint
		setPackageName(apkPath);//load apk package name		

	}
	
	private static void setPackageName(String apkPath) 
	{
		String packageName = null;
		
		String cmd = "tools/aapt dump permissions " + apkPath;
		
		try {
			//System.out.println("cmd:" + cmd);
			Process p = Runtime.getRuntime().exec(cmd);
			
			int processExitCode = p.waitFor();
			if (processExitCode != 0) 
			{
				throw new RuntimeException("Something went wrong during acquire apk permission(tools:aapt)");
			}
			
			packageName = aaptResult(p);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e){
			e.printStackTrace();
		}
		
		ApkInformation.packageName = packageName;
	}
	
	private static String aaptResult(Process p) throws IOException
	{
		
		String line;
		String packageName = null;
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		while ((line = input.readLine()) != null) 
		{
		  if (line.startsWith("package:"))
		  {
			  packageName = line.substring(line.indexOf(":")+2);
			  System.out.println(".......> packageName:" + packageName);
			  break;
		  }
		}
		input.close();
		
		return packageName;
	}

	private static void setFingerprint() 
	{
		String pemCert = "tools/apk-sign/testkey.x509.pem";
		
		 try {
			CertificateFactory certificatefactory=CertificateFactory.getInstance("X.509");
			FileInputStream bais=new FileInputStream(pemCert);
			X509Certificate cert = (X509Certificate)certificatefactory.generateCertificate(bais);
			
			byte[] myCert = cert.getEncoded();
	        MessageDigest digest = null;
			digest = java.security.MessageDigest.getInstance("MD5");
	        digest.update(myCert);
	        byte messageDigest[] = digest.digest();
	        
	         // Create Hex String
	         StringBuffer hexString = new StringBuffer();
	         for (int i=0; i<messageDigest.length; i++)
	         {
	        	 hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
	         }
	         
	         System.out.println("java_cert:" + hexString.toString());
	         ApkInformation.fingerprint = hexString.toString();
	         
		} catch (CertificateException | FileNotFoundException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
}
