package com.instrument;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.control.Control;
import com.util.SensitivePermission;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.PatchingChain;
import soot.RefType;
import soot.Scene;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.IntConstant;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.StringConstant;
import soot.jimple.VirtualInvokeExpr;
import soot.util.Chain;

public class MyBodyTransformer extends BodyTransformer
{	
	private final static int INSTRUMENT_Syetem_Out_Print = 			0x1;
	private final static int INSTRUMENT_Android_LOGD = 				0x2;
	private final static int INSTRUMENT_Alert_Dialog  = 			0x4;
	private final static int INSTRUMENT_Toast = 					0x8;
	private final static int INSTRUMENT_Get_STACK_INFO = 			0x10;
	private final static int INSTRUMENT_Anti_Debug_Flag = 			0x20;
	private final static int INSTRUMENT_Anti_Emulator_Flag =		0x40;
	private final static int INSTRUMENT_Cert_Authentication_Flag =  0x80;

	@Override
	protected void internalTransform(Body body, String phaseName,  Map<String, String> arg2) 
	{
		PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> i = units.snapshotIterator();
		while (i.hasNext())
		{
			Unit u = i.next();
			//System.out.println("--->" + u.toString());
			
			if ( isSensitiveApi(u) )
			{
				System.out.println("........> Warn: find sensitive api");
				System.out.println("........>" + u.toString());
				resolveMeasure(u, body);
				if ( isReadDatabase(u) )
				{
					replaceDatebase(u, body);
				}
			}
		}
	}

	
	private void replaceDatebase(Unit u, Body body) 
	{
		
	}


	private boolean isReadDatabase(Unit u) 
	{
		boolean flag = false;
		String readDatebaseApi = "<android.content.ContentResolver: android.database.Cursor query"
				+ "(android.net.Uri,java.lang.String[],java.lang.String,java.lang.String[],java.lang.String)>";
		
		if (u.toString().contains(readDatebaseApi))
		{
			System.out.println("warn: This statement reading database.");
			flag = true;
		}
			
		return flag;
	}

	private void resolveMeasure(Unit u, Body body) 
	{
		//boolean[] flags = Control.getFlagArray();
		if (u.toString().contains("<android.telephony.SmsManager: void sendTextMessage"
				+ "(java.lang.String,java.lang.String,java.lang.String,android.app.PendingIntent,android.app.PendingIntent)>")) {
			if (Control.isCheckSendMessage) {
				if ((Control.MEASURE & INSTRUMENT_Syetem_Out_Print) > 0) 
				{
					//System.out.println("-----> instrumentSystemOutPrintln");
					InstrumentMethod.instrumentSystemOutPrintln(u, body, "------> Warn:Get Sensitive Api");
				}
				
				if ((Control.MEASURE & INSTRUMENT_Android_LOGD) > 0)
				{
					//System.out.println("-----> instrumentAndroidLOG");
					InstrumentMethod.instrumentAndroidLOG(u, body);
				}
				
				if ((Control.MEASURE & INSTRUMENT_Alert_Dialog) > 0)
				{
					//System.out.println("-----> instrumentAlertDialog");
					InstrumentMethod.instrumentAlertDialog(u, body);
				}
				
				if ((Control.MEASURE & INSTRUMENT_Toast) > 0)
				{
					//System.out.println("-----> instrumentToast");
					InstrumentMethod.instrumentToast(u, body);
				}
			}
		}
		
	
		
		
		
	}
	
	private boolean isSensitiveApi(Unit u) 
	{
		boolean flag = false;
		
		ArrayList<String> sensitiveAPIArray;
		sensitiveAPIArray = SensitivePermission.getSensitiveApiList();
		
		Iterator<String> sAList = sensitiveAPIArray.iterator();
		//SensitivePermission.printArrayList(sensitiveAPIArray);
		while (sAList.hasNext())
		{
			String sA = sAList.next();
			if (u.toString().contains(sA))
			{
				flag = true;
				break;
			}
		}
		
		return flag;
	}
}
