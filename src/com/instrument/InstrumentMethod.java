package com.instrument;

import com.util.ApkInformation;

import soot.Body;
import soot.Local;
import soot.PatchingChain;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.IntConstant;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.VirtualInvokeExpr;
import soot.util.Chain;

public class InstrumentMethod 
{
	private static SootClass instrumentClass;
	
	static 
	{
		instrumentClass = Scene.v().loadClassAndSupport("com.util.InstrumentHelpClass");
	}
	
	/*
	 * The certificate fingerprint authentication
	 */
	public static void instrumentCertAuthentication(Unit u, Body body)
	{
		ApkInformation.getApkInfo(Main.apkPath);//load apk information
		
		String packageName = ApkInformation.getPackageName();
		String fingerprint = ApkInformation.getFingerprint();
		
		SootMethod certAuthentication = instrumentClass.getMethodByName("certFingerprintAuthentication");
		
		System.out.println("------->" + certAuthentication.getSignature());//debug it
		
		InvokeExpr invokeExpr= Jimple.v().newStaticInvokeExpr(certAuthentication.makeRef(), 
															body.getThisLocal(),
															StringConstant.v(packageName),
															StringConstant.v(fingerprint));
		
		Stmt invoketStmt = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertBefore(invoketStmt, u);
	}
	
	/*
	 * instrument anti 
	 */
	public static void instrumentAntiDebug(Unit u, Body body)
	{
		SootMethod antiDebug = instrumentClass.getMethodByName("antiDebug");
		
		System.out.println("------->" + antiDebug.getSignature());//debug it
		
		InvokeExpr invokeExpr= Jimple.v().newStaticInvokeExpr(antiDebug.makeRef(), body.getThisLocal());
		
		Stmt invoketStmt = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertBefore(invoketStmt, u);
	}
	
	/*
	 * instrument anti emulator
	 */
	public static void instrumentAntiEmulator(Unit u, Body body)
	{
		SootMethod antiEmulator = instrumentClass.getMethodByName("isRunningInEmualtor");
		
		System.out.println("------->" + antiEmulator.getSignature());//debug it
		
		InvokeExpr invokeExpr= Jimple.v().newStaticInvokeExpr(antiEmulator.makeRef());
		
		Stmt invoketStmt = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertBefore(invoketStmt, u);
	}
	
	/*
	 * instrument a alert dialog with a instrument help class
	 */
	public static void instrumentAlertDialog(Unit u, Body body)
	{
		SootMethod alertDialog = instrumentClass.getMethodByName("instrumentAlertDialog");
		
		System.out.println("------->" + alertDialog.getSignature());//debug it
		
		InvokeExpr invokeExpr= Jimple.v().newStaticInvokeExpr(alertDialog.makeRef(), body.getThisLocal());
		
		Stmt invoketStmt = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertBefore(invoketStmt, u);
	}
	
	/*
	 * instrument Android LOG
	 */
	public static void instrumentAndroidLOG(Unit u, Body body)
	{
		SootMethod sm = Scene.v().getMethod("<android.util.Log: int i(java.lang.String,java.lang.String)>");
		Value logType = StringConstant.v("INFO-YUYIFEI");
		Value logMessage = StringConstant.v("------> Warn: Find Sensitive Api");
		
		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(), logType, logMessage);
		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertBefore(generated, u);
	}
	
	/*
	 * instrument a toast
	 */
	public static void instrumentToast(Unit u, Body body)
	{
		//get a toast method
		SootMethod sm = Scene.v().getMethod("<android.widget.Toast: android.widget.Toast makeText"
				+ "(android.content.Context,java.lang.CharSequence,int)>");
	
		System.out.println("--------------> " + sm.getSignature());	//debug it
	    
		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(),
														body.getThisLocal(),//get this parameter
														StringConstant.v("新年好aaasyyyy"),
														IntConstant.v(0));
		
		System.out.println(invokeExpr.toString());//debug it
		
		//creat a local
		Local base = Jimple.v().newLocal("tmpRef", invokeExpr.getType());
		body.getLocals().add(base);
		//add a assign
		body.getUnits().insertBefore(Jimple.v().newAssignStmt(base, invokeExpr), u);
		
		SootMethod smShow = Scene.v().getMethod("<android.widget.Toast: void show()>");
		body.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(base, smShow.makeRef())), u);
	}
	
	
	/*
	 * instrument System.out.println
	 */
	public static void  instrumentSystemOutPrintln(Unit u, Body body, String info)
	{
		PatchingChain<Unit> units = body.getUnits();
		Chain<Local> locals = body.getLocals();
		
        // Add locals, java.io.printStream tmpRef
        Local tmpRef = Jimple.v().newLocal("tmpRef", RefType.v("java.io.PrintStream"));
        locals.add(tmpRef);
        
        {
        	//get a static field ref
        	StaticFieldRef field = Jimple.v().newStaticFieldRef(Scene.v()
        			.getField("<java.lang.System: java.io.PrintStream out>").makeRef());
        	//create a newAssignStmt
        	AssignStmt assign = Jimple.v().newAssignStmt(tmpRef, field);
        	//instrument a assign statement
        	units.insertBefore(assign, u);
        }
		
        // insert "tmpRef.println(info)"
        {
            SootMethod toCall = Scene.v().getMethod("<java.io.PrintStream: void println(java.lang.String)>");
            System.out.println("----->" + toCall.getSignature());
            
            // create a invokeExpr
            VirtualInvokeExpr invokeExpr = Jimple.v().newVirtualInvokeExpr(tmpRef, 
														        		toCall.makeRef(),
														        		StringConstant.v(info));
            //create a invokeStmt
            InvokeStmt invokeStmt = Jimple.v().newInvokeStmt(invokeExpr);
            //instrument a invokeStmt
            units.insertBefore(invokeStmt, u);
        }      
	}
}
