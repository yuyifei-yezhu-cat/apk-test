package com.control;

import java.util.ArrayList;

public class Control {
	   public static boolean isCheckSendMessage = false;
	   public static int MEASURE = 7;
	   public static boolean isCheckLocation = false;
	   public static String fakeLocation = "12345";
	   
//	   public static boolean INSTRUMENT_Syetem_Out_Print = false;
//	   public static boolean INSTRUMENT_Alert_Dialog = false;
//	   public static boolean INSTRUMENT_Toast = false;
	   
	   public static boolean[] getFlagArray()
	   {
		   ArrayList<Boolean> flagList = new ArrayList <Boolean>();
		   
		   flagList.add(isCheckSendMessage);
		   flagList.add(isCheckLocation);
		   
		   Boolean[] flagArray = flagList.toArray(new Boolean[flagList.size()]);
		   
		   boolean[] flags = new boolean[flagArray.length];
		   for (int i=0; i<flagArray.length; ++i)
		   {
			   flags[i] = flagArray[i];
		   }
		   
		   return flags;
	   }
	}
