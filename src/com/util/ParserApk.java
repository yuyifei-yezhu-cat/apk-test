package com.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;

public class ParserApk
{
	public static File adaptManifest(String apkFile, HashMap<String, String> replacementMap) 
	{
		String manifestString = null;
		try {
			manifestString = decodeApk(new File(apkFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(manifestString);
		
		//System.out.println("===============================");
		//System.out.println("===============================");
		
		for (String key:replacementMap.keySet())
		{
			assert replacementMap.get(key)!=null;
			manifestString = manifestString.replaceFirst(key, replacementMap.get(key));
		}
		
		System.out.println(manifestString);
		
		File newFile = null;
		try {
			newFile = replaceManifest(new File(apkFile), manifestString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return newFile;
	}
	
	private static File replaceManifest(File apkFile, String newManifest) throws IOException, InterruptedException 
	{
		File decodedDir = new File ("decoded");
		if (decodedDir.exists() && decodedDir.isDirectory()) 
		{
			FileUtils.deleteDirectory(decodedDir);
		}
		
		Process p = Runtime.getRuntime().exec(
				"java -jar tools/apktool.jar d -s -f " + apkFile.getAbsolutePath() + " -o decoded");
		int processExitCode = p.waitFor();
		if (processExitCode != 0)
		{
			System.out.println("Something went wrong when unpacking " + apkFile.getAbsolutePath());
			return null;
		}
		
		// Replace content of AndroidManifest.xml
		FileOutputStream fos = new FileOutputStream("decoded/AndroidManifest.xml");
		fos.write(newManifest.getBytes());
		fos.close();
		
		// Now pack again
		File newFile = new File(apkFile.getAbsolutePath().replace(".apk", "") + "_modified.apk");
		if (newFile.exists())
		{
			newFile.delete();
		}
		
		p = Runtime.getRuntime().exec("java -jar tools/apktool.jar b decoded/ -o " + newFile.getAbsolutePath());
		processExitCode = p.waitFor();
		if (processExitCode != 0)
		{
			System.err.println("Something went wrong when packing " + apkFile.getAbsolutePath());
			return null;
		}
		
		return newFile;
	}

	public static String decodeApk(File apkFile) throws IOException, InterruptedException
	{
		String manifest = null;
		
		File decodedDir = new File ("decoded");
		if (decodedDir.exists() && decodedDir.isDirectory()) 
		{
			FileUtils.deleteDirectory(decodedDir);
		}
		
		System.out.println("--->" + apkFile.getAbsolutePath());
		
		Process p = Runtime.getRuntime().exec(
				"java -jar tools/apktool.jar d -s -f " + apkFile.getAbsolutePath() + " -o decoded");
		
		System.out.println("java -jar tools/apktool.jar d -s -f " + apkFile.getAbsolutePath() + " -o decoded");
		
		int processExitCode = p.waitFor();
		
		if (processExitCode != 0) 
		{
			throw new RuntimeException("Something went wrong during unpacking");
		}
		
		BufferedReader br = new BufferedReader(new FileReader("decoded/AndroidManifest.xml"));
		StringBuffer sb = new StringBuffer();
		String line = "";
		while ((line = br.readLine()) != null) 
		{
			sb.append(line);
			sb.append("\n");
		}
		br.close();
		manifest = sb.toString();
		
		return manifest;
	}
}
