package com.util;

public class Permission {
	private String permissionName;

	private String CN_Name;//Chinese name
	
	private String[] sensitiveApi;

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getCN_Name() {
		return CN_Name;
	}

	public void setCN_Name(String cN_Name) {
		CN_Name = cN_Name;
	}

	public String[] getSensitiveApi() {
		return sensitiveApi;
	}

	public void setSensitiveApi(String[] sensitiveApi) {
		this.sensitiveApi = sensitiveApi;
	}
}
