package com.control;

import java.util.LinkedList;

import com.control.args.ArgType;

public class ArgumentParser {
	public static LinkedList<args> allArgs;
	public ArgumentParser()
	{
		    allArgs = new LinkedList<args>();
		    allArgs.add(new args("message", "<n>", "检查apk是否发送短信,检查等级有0-31", ArgType.ARGTYPE_INT, 
		    		new F() {
						@Override
						public void f(Object o) {
							Control.isCheckSendMessage = true;
							int s = (int) o;
							if(s >= 0 && s <= 31) {
								Control.MEASURE = s;
							}
							else {
								System.out.println("命令-message 无效参数 \"" + s + "\"");
								System.exit(0);
							}
						}
			}));
		    allArgs.add(new args("location", "<s>", "检查apk是否查看用户位置，可设置虚假位置", ArgType.ARGTYPE_STRING, 
		    		new F() {
						@Override
						public void f(Object o) {
							Control.isCheckLocation = true;
							String s = (String) o;
							if(!s.equals(""))
							   Control.fakeLocation = s;
						}
			}));
		    allArgs.add(new args("help", "", "查看所有命令", ArgType.ARGTYPE_EMPTY, 
		    		new F() {
						@Override
						public void f(Object o) {
							ArgPrint();
							System.exit(1);
						}
			}));
		/*	allArgs[0] = new args();
			allArgs[0].argName = "message";
			allArgs[0].argDisplay = "<n>";
			allArgs[0].argDescription = "检查apk是否发送短信";
			allArgs[0].argType = ArgType.ARGTYPE_INT;
			allArgs[0].action = new F() {
				@Override
				public void f(Object o) {
					Control.isCheckSendMessage = true;
					int s = (int) o;
					if(s == 1 || s == 2 || s == 3) {
						Control.messageLevel = s;
					}
				}
			};*/
			// System.out.println(allArgs[0].toString());
		/*	allArgs[1] = new args();
			allArgs[1].argName = "location";
			allArgs[1].argDisplay = "<s>";
			allArgs[1].argDescription = "检查apk是否查看用户位置，可设置虚假位置";
			allArgs[1].argType = ArgType.ARGTYPE_STRING;
			allArgs[1].action = new F() {
				@Override
				public void f(Object o) {
					Control.isCheckLocation = true;
					String s = (String) o;
					if(!s.equals(""))
					   Control.fakeLocation = s;
				}
			};*/
	}
	public static void ArgPrint(){
		for(args a : allArgs) {
		    System.out.println(a.toString());
		}
	}
	public static void errorNoArgument(String s) {
		System.out.println("Error: " + s + ": requires an argument");
		ArgPrint();
        System.exit(1);
	}
	public static void errorInvalidOption(String s){
		System.out.println("invalid option: " + s);
		ArgPrint();
		System.exit(1);
	}
	public void parser(String[] args)
	{
		for (int i = 0; i < args.length; i++) {
			String s = args[i];
			if(!s.startsWith("-")) {
				errorInvalidOption(s);
			}				
			boolean found = false;
			for (args a : allArgs) {
				if (!s.equals("-" + a.argName)) {
					continue;
				}
				found = true;
				switch (a.argType) {
				    case ARGTYPE_EMPTY: {
					   a.action.f(null);
					   break;
				    }
				    default: {
				    	if(i >= args.length - 1) {
				    		errorNoArgument(args[i]);
				    	}
				    	i++;
					    break;
				    }
				}
				String theOPsArg = args[i];;
				switch (a.argType) {
					case ARGTYPE_BOOL: {
						break;
					}
					case ARGTYPE_INT: {
						int num = -9999;// unreachable
		                try {
		        		    num = Integer.parseInt(theOPsArg);
		        		} catch (java.lang.NumberFormatException e) {
		        		     System.out.println("Error: -" + a.argName + ": requires an integer argument");
		        		     ArgPrint();
		        		     System.exit(1);
		        		}	               
				        a.action.f(num);
				        break;
					}
					case ARGTYPE_STRING: {			
						a.action.f(theOPsArg);
						break;
					}
					default: {
						break;
					}
				}			
			}
			if(found == false) {
				errorInvalidOption(s);
			}
		}
	}
}
